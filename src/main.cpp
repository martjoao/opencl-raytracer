#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>

#include "utils.h"
#include "clhost.h"
#include "sphere.h"
#include "ray.h" 
#include "image.h"
#include "constants.h"


int main(void) {
	cl_int err = 0;
	
	//Setup OpenCL
	cl_platform_id platform = getPlatforms();
	cl_device_id device = getDevices(platform, CL_DEVICE_TYPE_GPU);

	cl_context_properties ctxProps[] =
	{
		CL_CONTEXT_PLATFORM, (cl_context_properties)platform,
		0, 0
	};
	cl_context ctx = clCreateContext(ctxProps, 1, &device, NULL, NULL, &err);
	cl_command_queue queue1 = clCreateCommandQueue(ctx, device, NULL, &err);

	//Setup Program
	std::string src = loadFile("kernel.cl");
	size_t srcSize = src.size();
	const char* srcs[1] = { src.data() };
	std::cout << srcs[0];
	//Build Program
	cl_program prog = clCreateProgramWithSource(ctx, 1, srcs, &srcSize, &err);
	err = clBuildProgram(prog, 1, &device, NULL, NULL, NULL);
	if (err < 0)
	{
		//PRINT BUILD ERROR
		size_t log_size;
		clGetProgramBuildInfo(prog, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char* log = (char*)calloc(log_size + 1, sizeof(char));
		clGetProgramBuildInfo(prog, device, CL_PROGRAM_BUILD_LOG, log_size + 1, log, NULL);
		printf("%s/n", log);
		free(log);
		std::cin >> err;
		return 1;
	}

	//Setup Kernel
	cl_kernel krn = clCreateKernel(prog, "rayTracer", &err);

	//Setup host data
	std::vector<Sphere> spheres(NUM_SPHERES);
	for (int i = 0; i < NUM_SPHERES; i++)
	{
		/*
		Sphere s = createSphere(
			glm::vec3((rand() % WIDTH) - (WIDTH/2), (rand() % HEIGHT) - (HEIGHT/2), (rand() % 10) - 11),
			(float)(rand()%30 + 10),
			glm::vec3(rand() % 200 + 55, rand() % 200 + 55, rand() % 200 + 55)); //at least a little grey
		//*/

		//*
		Sphere s = createSphere(
			glm::vec3((i - NUM_SPHERES/2)*60, 0, -3),
			(float)30,
			glm::vec3(rand() % 200 + 55, rand() % 200 + 55, rand() % 200 + 55),
			createBRDF(0.2, 0.4, 0.4, 5, false)); //at least a little grey
		//*/
		spheres[i] = s;
	}
	spheres[2].props.reflect = true;

	//Setup device data
	//cl_image_format fmt;
	//fmt.image_channel_order = CL_RGBA;
	//fmt.image_channel_data_type = CL_UNSIGNED_INT8;

	cl_mem spheresBuff = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, spheres.size() * sizeof(Sphere), spheres.data(), &err);
	//cl_mem resBuff = clCreateImage2D(ctx, CL_MEM_WRITE_ONLY, &fmt, WIDTH, HEIGHT, 0, NULL, &err);
	cl_mem resBuff = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY, WIDTH * HEIGHT * sizeof(char) * 3,NULL, &err);

	//Setup kernel arguments
	err = clSetKernelArg(krn, 0, sizeof(cl_mem), (void*)&spheresBuff);
	err = clSetKernelArg(krn, 1, sizeof(cl_mem), (void*)&resBuff);

	//Run kernel
	size_t gSize[] = { WIDTH, HEIGHT };
	err = clEnqueueNDRangeKernel(queue1, krn, 2, NULL, gSize, NULL, 0, NULL, NULL);

	//Read result
	//Image img = createRGBAImage(WIDTH, HEIGHT);
	Image img = createRGBImage(WIDTH, HEIGHT);

	//size_t origin[] = { 0, 0, 0 };
	//size_t region[] = { WIDTH , HEIGHT , 1 };
	//err = clEnqueueReadImage(queue1, resBuff, CL_TRUE, origin, region, 0, 0, img.pixel.data(), 0, NULL, NULL);
	err = clEnqueueReadBuffer(queue1, resBuff, CL_TRUE, 0, sizeof(char)* WIDTH * HEIGHT * 3, img.pixel.data(), 0, NULL, NULL);

	//Write result to image file
	std::cout << "writing to file result.ppm" << std::endl;
	//SaveImage(RGBAtoRGB(img), "result.ppm");
	SaveImage(img, "resultT.ppm");

	//Cleanup
	err = clFlush(queue1);
	err = clFinish(queue1);
	err = clReleaseKernel(krn);
	err = clReleaseProgram(prog);
	err = clReleaseMemObject(spheresBuff);
	err = clReleaseMemObject(resBuff);
	err = clReleaseCommandQueue(queue1);
	err = clReleaseContext(ctx);
	std::cout << sizeof (cl_float3) << std::endl;
	std::cout << sizeof (cl_float4) << std::endl;
	std::cout << sizeof (glm::vec3) << std::endl;
	std::cout << sizeof (glm::vec4) << std::endl;
	std::cout << sizeof (Sphere) << std::endl;
	int i;

	std::cin >> i;



	return 0;
}