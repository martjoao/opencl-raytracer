#include "utils.h"
#include <fstream>

std::string loadFile(std::string filepath)
{
	std::ifstream in(filepath);
	std::string result(
		(std::istreambuf_iterator<char>(in)),
		std::istreambuf_iterator<char>());
	return result;
}
