#include "clhost.h"
#include <iostream>

std::string GetPlatformName(cl_platform_id id)
{
	size_t size = 0;
	clGetPlatformInfo(id, CL_PLATFORM_NAME, 0, nullptr, &size);

	std::string result;
	result.resize(size);
	clGetPlatformInfo(id, CL_PLATFORM_NAME, size,
		const_cast<char*> (result.data()), nullptr);

	return result;
}

std::string GetDeviceName(cl_device_id id)
{
	size_t size = 0;
	clGetDeviceInfo(id, CL_DEVICE_NAME, 0, nullptr, &size);

	std::string result;
	result.resize(size);
	clGetDeviceInfo(id, CL_DEVICE_NAME, size,
		const_cast<char*> (result.data()), nullptr);

	return result;
}

cl_platform_id getPlatforms()
{
	cl_uint nPlatforms;
	cl_int ret = clGetPlatformIDs(0, NULL, &nPlatforms);

	if (nPlatforms == 0)
	{
		std::cout << "NO PLATFORMS FOUND" << std::endl;
		return NULL;
	}

	std::vector<cl_platform_id>* platforms = new std::vector<cl_platform_id>(nPlatforms);
	clGetPlatformIDs(nPlatforms, platforms->data(), NULL);

	std::cout << "Chose a platform" << std::endl;
	for (cl_uint i = 0; i < nPlatforms; i++)
	{
		std::cout << i + 1 << ": " << (GetPlatformName(platforms->at(i))) << std::endl;
	}

	int chosen;
	std::cin >> chosen;
	if (chosen <= 0 || chosen > (int)nPlatforms) return (cl_platform_id) -1;
	chosen--;
	return platforms->at(chosen);
}

cl_device_id getDevices(cl_platform_id plat, cl_device_type type)
{
	cl_uint nDevs;
	cl_int ret = clGetDeviceIDs(plat, type, 0, NULL, &nDevs);

	if (nDevs == 0)
	{
		std::cout << "NO DEVICES FOUND";
		return NULL;
	}

	std::vector<cl_device_id>* devices = new std::vector<cl_device_id>(nDevs);
	clGetDeviceIDs(plat, type, nDevs, devices->data(), NULL);

	std::cout << "Chose a device" << std::endl;
	for (cl_uint i = 0; i < nDevs; i++)
	{
		std::cout << i + 1 << ": " << (GetDeviceName(devices->at(i))) << std::endl;
	}

	int chosen;
	std::cin >> chosen;
	if (chosen <= 0 || chosen >(int)nDevs) return (cl_device_id)-1;
	chosen--;
	return devices->at(chosen);
}

