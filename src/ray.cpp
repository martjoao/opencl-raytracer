#include "ray.h"

Ray createRay(glm::vec3 pos, glm::vec3 dir)
{
	Ray r;
	r.origin = pos;
	r.direction = dir;
	return r;
}
