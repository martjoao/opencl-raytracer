#include "BRDF.h"

BRDF createBRDF(float ka, float kd, float ks, float alpha, bool reflect)
{
	BRDF x;
	x.ka = ka;
	x.kd = kd;
	x.ks = ks;
	x.alpha = alpha;
	x.reflect = reflect;
	return x;
}
