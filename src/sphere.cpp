#include "sphere.h"

Sphere createSphere(glm::vec3 pos, float radius, glm::vec3 colour, BRDF props)
{
	Sphere c;
	c.x = pos[0];
	c.y = pos[1];
	c.z = pos[2];
	//c.pos = pos;
	c.radius = radius;
	c.r = colour[0];
	c.g = colour[1];
	c.b = colour[2];
	//c.color = colour;
	c.props = props;
	return c;
}
