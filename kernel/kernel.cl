#include "constants.h"

//BRDF struct -> lighting properties
typedef struct BRDF {
	float ka, kd, ks, alpha;
	bool reflect;
}BRDF;

//Sphere Struct
typedef struct Sphere {
	float x, y, z;
	float radius;
	float  r, g, b;
	BRDF props;
}Sphere;

float3 normalAt(__global Sphere* s, float3 location)
{
	float3 normal = (location - (float3)(s->x, s->y, s->z))/s->radius;
	return normal;
}

int3 shade(float3 pos, float3 normal, int3 color, __global BRDF* props)
{
	//light_pos will become a kernel argument -> lights
	float3 lightPos = (float3)(0,0,40);
	//camPos will become a kernel argument
	float3 camPos = (float3)(0,0,250);
	
	float3 lightDir = normalize(lightPos - pos);
	float dotLN = dot(lightDir, normal);
	float3 ref = 2 * dotLN*normal - lightDir;

	//phong reflection model
	float ambient = props->ka;
	float diffuse = props->kd * max(0.f, dotLN);
	float specular = props->ks * pow(max(0.f, dot(ref, normalize(camPos - pos))), props->alpha);
	float i = ambient + diffuse + specular;

	float3 c = i * (float3)(color.s0, color.s1, color.s2);
	return (int3)(min(c.s0, 255.f), min(c.s1, 255.f), min(c.s2, 255.f));
}

//Ray Struct
typedef struct Ray
{
	float3 origin;
	float3 direction;
}Ray;

Ray createRay(float3 pos, float3 dir)
{
	Ray r;
	r.origin = pos;
	r.direction = normalize(dir);
	return r;
}


//Orthogonal Projection
/*
float hit(__global Sphere* s, float ox, float oy, float *n) {
	float radius = s->radius;
	float dx = ox - s->x;
	float dy = oy - s->y;
	if (dx*dx + dy*dy < radius*radius) {
		float dz = sqrt(radius*radius - dx*dx - dy*dy);
		*n = dz / sqrt(radius * radius);
		return dz + s->z;
	}
	return -INF;
}

int3 rayTrace( __global Sphere* spheres, float ox, float oy)
{
	int3 ret = (int3)(0,0,0);

	float   maxz = (float)-INF;
	float   r = 0, g = 0, b = 0;

	for (int i = 0; i<NUM_SPHERES; i++)
	{
		float   n;
		float   t = hit(&(spheres[i]), ox, oy, &n);
		if (t > maxz)
		{
			float fscale = n;
			r = spheres[i].r * fscale;
			g = spheres[i].g * fscale;
			b = spheres[i].b * fscale;
		}
	}

	ret.s012 = (int3)(r,g,b);
	return ret;
}
*/

//Perspective projection
bool intersect(__global Sphere* s, Ray *r, float* t)
{
	//* Method 1
	float3 L = (float3)(s->x, s->y, s->z) - r->origin;
	float tca = dot(L, r->direction);
	float radius2 = s->radius*s->radius;
	if (tca < 0) return false;
	float d2 = dot(L,L) - tca * tca;
	if (d2 > radius2) return false;
	float thc = sqrt(radius2 - d2);
	float t1 = tca - thc;
	float t2 = tca + thc;
	//*/

	
	/*	Method 2
	//common values
	float3 l = r->direction;
	float3 o = r->origin;
	float3 center = (float3)(s->x, s->y, s->z);
	float rad = s->radius;

	//quad equation values
	float a = dot(l, l);
	float b = 2.0f * (dot(l, o - center));
	float c = dot(o - center, o - center) - rad * rad;
	float disc = b*b - 4.0f*a*c;

	if (disc < 0) return false;

	float sqd = sqrt(disc);
	float t1 = (-b + sqd) / 2 * a;
	float t2 = (-b - sqd) / 2 * a;
	//*/

	//T1 and T2 evaluation
	if (t1 < 0 && t2 < 0) 
		return false;
	else if (t1 < 0)
		*t = t2;
	else if (t2 < 0)
		*t = t1;
	else
		*t = (t1 < t2) ? t1 : t2;
	return true;
	
}

//trace ray iteratiely (recursion not supported)
int3 iterativeRayTrace(__global Sphere* spheres, Ray* ray)
{
	int3 ret = (int3)(0, 0, 0);
	Ray r;
	r.origin = ray->origin;
	r.direction = ray->direction;

	while (true)
	{
		float   closest = (float)INF;
		__global Sphere* closestSphere = NULL;

		for (int i = 0; i < NUM_SPHERES; i++)
		{
			float t = 0;
			if (!intersect(&(spheres[i]), &r, &t))
				continue;

			if (t < closest)
			{
				closest = t;
				closestSphere = &(spheres[i]);
			}
		}
		if (closest > 9999) return ret;

		float3 location = r.origin + (r.direction * closest);
		float3 normal = normalAt(closestSphere, location);
		int3 color = (int3)(closestSphere->r, closestSphere->g, closestSphere->b);

		if (closestSphere->props.reflect)
		{
			//create reflected ray
			float3 r1 = -r.direction;
			float3 ref = 2 * dot(r1, normal)*normal - r1;

			r.origin = location;
			r.direction = normalize(ref);

			continue;
			//trace it (RECURSION)
		}
		else
			return shade(location, normal, color, &(closestSphere->props));
	}
	return ret;
}


__kernel void rayTracer(__global Sphere* spheres, __global char* res) 
{
	// Get the index of the current element to be processed
	int x = get_global_id(0);
	int y = get_global_id(1);
	
	float ox = x - WIDTH / 2;
	float oy = y - HEIGHT / 2;

	float3 origin = (float3)(0, 0, 250);
	float3 dir = (float3)(ox, oy, 0) - origin;

	Ray r = createRay(origin, dir);

	int3 color = iterativeRayTrace(spheres, &r);

	res[x * 3 + y * WIDTH * 3 + 0] = color.s0;
	res[x * 3 + y * WIDTH * 3 + 1] = color.s1;
	res[x * 3 + y * WIDTH * 3 + 2] = color.s2;
}