#ifndef _INCLUDE_UTILS_H_
#define _INCLUDE_UTILS_H_

#include <string>

std::string loadFile(std::string filepath);

#endif
