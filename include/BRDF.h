#ifndef _INCLUDE_BRDF_H_
#define _INCLUDE_BRDF_H_

//NOT A REAL BRDF, lighting info

#include <glm\glm.hpp>

//lighting properties
typedef struct BRDF {
	float ka, kd, ks, alpha;
	bool reflect;
}BRDF;

BRDF createBRDF(float ka, float kd, float ks, float alpha, bool reflect);

#endif