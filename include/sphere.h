#ifndef _INCLUDE_SPHERE_H_
#define _INCLUDE_SPHERE_H_

#include <glm\glm.hpp>
#include "BRDF.h"


typedef struct Sphere {
	float x, y, z;
	float radius;
	float  r, g, b;
	BRDF props;
}Sphere;

Sphere createSphere(glm::vec3 pos, float radius, glm::vec3 colour, BRDF props);

#endif