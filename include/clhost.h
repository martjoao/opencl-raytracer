#ifndef _INCLUDE_CLHOST_H_
#define _INCLUDE_CLHOST_H_

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <vector>
#include <string>

//OpenCL-based functions to simplify setup

std::string GetPlatformName(cl_platform_id id);
std::string GetDeviceName(cl_device_id id);
cl_platform_id getPlatforms();
cl_device_id getDevices(cl_platform_id plat, cl_device_type type);
cl_context createContext();

#endif