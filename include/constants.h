#ifndef _INCLUDE_CONSTANTS_H_
#define _INCLUDE_CONSTANTS_H_

//constants -> included by both host and devices

#define NUM_SPHERES 5
#define WIDTH	1000
#define HEIGHT	700
#define INF 0xffffffff
#define MAX_ITERATIONS 3;

#endif