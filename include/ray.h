#ifndef _INCLUDE_RAY_H_
#define _INCLUDE_RAY_H_

#include <glm\glm.hpp>

typedef struct Ray
{
	glm::vec3 origin;
	glm::vec3 direction;
}Ray;

Ray createRay(glm::vec3 pos, glm::vec3 dir);

#endif