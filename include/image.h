#ifndef _INCLUDE_IMAGE_H_
#define _INCLUDE_IMAGE_H_

#include <vector>
#include <glm/glm.hpp>

typedef struct Image
{
	std::vector<char> pixel;
	int width, height;
}Image;

Image createRGBImage(int w, int h);
Image createRGBAImage(int w, int h);

Image LoadImage(const char* path);
void SaveImage(const Image& img, const char* path);

Image RGBtoRGBA(const Image& input);
Image RGBAtoRGB(const Image& input);

#endif